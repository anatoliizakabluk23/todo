- [About the Project](#markdown-header-about-the-project)
    - [Built With](#markdown-header-built-with)
- [Getting Started](#markdown-header-getting-started)
    - [Prerequisites](#markdown-header-prerequisites)
    - [Installation](#markdown-header-installation)
    - [Tests](#markdown-header-tests)
    - [Notices](#markdown-header-notices)
- [License](#markdown-header-license)

# About The Project

#### TodoList-FastAPI

Basic REST service for working with a TODO list.  
(P.S. It is my first steps in FastAPI. Please, don't judge strictly :blush:.)

### Built With
* [Python 3](https://www.python.org/)
* [FastAPI](https://fastapi.tiangolo.com)
* [Docker Compose](https://docs.docker.com/compose/)
* [PostgreSQL](https://www.postgresql.org/)

# Getting Started

### Prerequisites

* [Docker](https://docs.docker.com/engine/install/ubuntu/)
* [Docker Compose](https://docs.docker.com/compose/install/) 

### Installation

1.  Clone the repository:

```bash
git clone https://your_username_@bitbucket.org/anatoliizakabluk23/todo.git
```

2. Make project Environment:

    Update `.env` file in root directory

3. Build and run docker composer containers

```bash
docker-compose up -d --build
```
    
4. Open URL:
    
    `http://localhost:8002/docs`

### Tests

Run tests:

```bash
docker-compose exec web pytest
```

### Notices

* For verifies pep8, pyflakes and circular complexity run:

```bash
docker-compose exec web flake8
```

* During of building the `web` container, the script `initial_data.py` is loading test data into the database.
**The data is loaded one time.**

* For testing all users are created with password `root`.

* Authorization type: `HTTP Bearer`

# License

Distributed under the MIT License. See `LICENSE` for more information.
