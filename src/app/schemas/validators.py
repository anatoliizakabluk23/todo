def str_not_empty(str: str) -> str:
    assert str.replace(' ', '') != '', 'Empty string are not allowed'
    return str


def str_must_contain_space(str: str) -> str:
    if ' ' not in str:
        raise ValueError('Must contain a space')
    return str.title()


def str_alphanumeric(str: str) -> str:
    assert str.isalnum(), 'Must be alphanumeric'
    return str


def passwords_match(v, values, **kwargs):
    if v != values['new_password']:
        raise ValueError('Passwords do not match')
    return v
