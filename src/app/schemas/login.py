from pydantic import BaseModel, EmailStr


class HTTPBearerCredentials(BaseModel):
    email: EmailStr
    password: str
