from pydantic import BaseModel, Field
from geoalchemy2.shape import to_shape


class Coordinates(BaseModel):
    lat: float = Field(0, gte=-90, lte=90)
    lng: float = Field(0, gte=-180, lte=180)


class GeoJson(str):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        shply_geom = to_shape(v)

        return {'lng': shply_geom.x, 'lat': shply_geom.y}

    def __repr__(self):
        return f'Geos({super().__repr__()})'


class GeoWKTElement(Coordinates):
    @classmethod
    def __get_validators__(cls):
        yield cls.validate

    @classmethod
    def validate(cls, v):
        if 'lat' not in v or 'lng' not in v:
            raise ValueError('Latitude and Longitude should be set both')

        if not -90 <= float(v['lat']) <= 90:
            raise ValueError('Latitude outside allowed range')

        if not -180 <= float(v['lng']) <= 180:
            raise ValueError('Longitude outside allowed range')

        # WKTElement as string
        geom_wkte = f"SRID=4326;Point({v['lng']} {v['lat']})"

        return geom_wkte

    def __repr__(self):
        return f'Geos({super().__repr__()})'
