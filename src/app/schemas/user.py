from typing import Optional
from datetime import datetime

from pydantic import BaseModel, EmailStr, validator

from app.schemas.data_types import GeoJson, GeoWKTElement
from app.schemas.validators import (str_must_contain_space,
                                    str_alphanumeric, passwords_match)


class AddressBase(BaseModel):
    street: str
    suite: str
    city: str
    zipcode: str


class AddressItem(AddressBase):
    geo: GeoJson

    class Config:
        orm_mode = True


class AddressCreate(AddressBase):
    geo: Optional[GeoWKTElement] = None


class AddressUpdate(BaseModel):
    street: Optional[str] = None
    suite: Optional[str] = None
    city: Optional[str] = None
    zipcode: Optional[str] = None
    geo: Optional[GeoWKTElement] = None


class CompanyItem(BaseModel):
    name: str
    catchPhrase: str
    bs: str

    class Config:
        orm_mode = True


class CompanyCreate(BaseModel):
    name: Optional[str] = None
    catchPhrase: Optional[str] = None
    bs: Optional[str] = None


class CompanyUpdate(BaseModel):
    name: Optional[str] = None
    catchPhrase: Optional[str] = None
    bs: Optional[str] = None


class UserBase(BaseModel):
    email: EmailStr
    name: str
    username: str
    phone: str
    website: str


class UserCreate(UserBase):
    address: AddressCreate
    company: CompanyCreate
    password: str

    # validators
    _name_must_contain_space = validator(
        'name', allow_reuse=True)(str_must_contain_space)

    _username_alphanumeric = validator(
        'username', allow_reuse=True)(str_alphanumeric)


class UserUpdate(BaseModel):
    email: Optional[EmailStr] = None
    name: Optional[str] = None
    username: Optional[str] = None
    phone: Optional[str] = None
    website: Optional[str] = None
    address: Optional[AddressUpdate] = None
    company: Optional[CompanyUpdate] = None

    # validators
    _name_must_contain_space = validator(
        'name', allow_reuse=True)(str_must_contain_space)

    _username_alphanumeric = validator(
        'username', allow_reuse=True)(str_alphanumeric)


class UserItem(UserBase):
    id: int
    address: AddressItem
    company: CompanyItem
    created_at: datetime
    updated_at: Optional[datetime] = datetime.now()
    is_active: Optional[bool] = True
    is_superuser: bool = False

    class Config:
        orm_mode = True


class UserPasswordUpdate(BaseModel):
    old_password: str
    new_password: str
    new_password_repeat: str

    _passwords_match = validator(
        'new_password_repeat', allow_reuse=True)(passwords_match)
