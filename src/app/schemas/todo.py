from datetime import datetime
from typing import Optional

from pydantic import BaseModel, validator

from app.schemas.validators import str_not_empty


class TodoBase(BaseModel):
    title: str
    completed: bool = False


class TodoCreate(TodoBase):
    # validators
    _not_empty_title = validator(
        'title', allow_reuse=True)(str_not_empty)


class TodoItem(TodoBase):
    id: int
    user_id: int
    created_at: datetime
    updated_at: Optional[datetime] = datetime.now()

    class Config:
        orm_mode = True


class TodoUpdate(TodoBase):
    # validators
    _not_empty_title = validator(
        'title', allow_reuse=True)(str_not_empty)
