from typing import List, Any

from fastapi import HTTPException, status
from fastapi.routing import APIRouter
from fastapi.param_functions import Depends, Path
from sqlalchemy.orm import Session

from app import schemas, models, crud
from app.api import deps
from app.db.db import get_session
from app.core.security import verify_password


router = APIRouter()


@router.get("/", response_model=List[schemas.UserItem])
def read_users(
        db: Session = Depends(get_session),
        skip: int = 0,
        limit: int = 100
) -> Any:
    """
    Retrieve users.
    """
    users = crud.user.get_multi(db, skip=skip, limit=limit)

    return users


@router.get("/me", response_model=schemas.UserItem)
def read_user_me(
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get current user.
    """
    return current_user


@router.put("/me", response_model=schemas.UserItem)
def update_user_me(
        db: Session = Depends(get_session),
        *,
        payload: schemas.UserUpdate,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Update own user.
    """
    user = crud.user.update(db, db_obj=current_user, obj_in=payload)

    return user


@router.patch("/me/update-password/", response_model=schemas.Msg)
def reset_password(
        db: Session = Depends(get_session),
        *,
        payload: schemas.UserPasswordUpdate,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Update password.
    """
    if not verify_password(
            payload.old_password, current_user.hashed_password):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Incorrect current password")

    payload = {'password': payload.new_password}
    crud.user.update(db, db_obj=current_user, obj_in=payload)

    return {"msg": "Password updated successfully"}


@router.get("/{user_id}", response_model=schemas.UserItem)
def read_user_by_id(
        user_id: int = Path(..., ge=1),
        current_user: models.User = Depends(deps.get_current_active_user),
        db: Session = Depends(get_session),
) -> Any:
    """
    Get a specific user by id.
    """
    user = crud.user.get(db, id=user_id)
    if user == current_user:
        return user
    if not crud.user.is_superuser(current_user):
        raise HTTPException(
            status_code=status.HTTP_400_BAD_REQUEST,
            detail="The user doesn't have enough privileges"
        )

    return user
