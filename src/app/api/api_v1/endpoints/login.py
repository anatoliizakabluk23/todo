from typing import Any

from fastapi import APIRouter, Depends
from sqlalchemy.orm import Session

from app import schemas
from app.schemas.login import HTTPBearerCredentials
from app.db.db import get_session
from app.core import security

router = APIRouter()


@router.post("/login/access-token", response_model=schemas.Token)
def login_access_token(
        db: Session = Depends(get_session),
        *,
        credential: HTTPBearerCredentials
) -> Any:
    """
    HTTPBearer compatible token login, get an access token for future requests
    """
    access_token = security.authenticate(
        db, email=credential.email, password=credential.password
    )

    return {
        "access_token": access_token,
        "token_type": "bearer",
    }
