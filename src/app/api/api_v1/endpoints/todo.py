from typing import List, Any

from fastapi import HTTPException, status
from fastapi.routing import APIRouter
from fastapi.param_functions import Depends, Path
from sqlalchemy.orm import Session

from app import schemas, models, crud
from app.db.db import get_session
from app.api import deps


router = APIRouter()

__valid_id = Path(..., ge=1)


@router.get("/", response_model=List[schemas.TodoItem])
def read_todos(
        db: Session = Depends(get_session),
        skip: int = 0,
        limit: int = 100,
        completed: bool = False,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Retrieve todos.
    """
    if crud.user.is_superuser(current_user):
        todos = crud.todo.get_multi(db, skip=skip, limit=limit,
                                    filters={'completed': completed})
    else:
        todos = crud.todo.get_multi_by_owner(
            db=db, user_id=current_user.id, skip=skip,
            limit=limit, filters={'completed': completed}
        )

    return todos


@router.post("/", response_model=schemas.TodoItem)
def create_todo(
        *,
        db: Session = Depends(get_session),
        todo_in: schemas.TodoCreate,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Create new todo.
    """
    todo = crud.todo.create_with_owner(
        db=db, obj_in=todo_in, user_id=current_user.id
    )

    return todo


@router.put("/{id}", response_model=schemas.TodoItem)
def update_todo(
        *,
        db: Session = Depends(get_session),
        id: int = __valid_id,
        item_in: schemas.TodoUpdate,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Update an todo.
    """
    todo = crud.todo.get(db=db, id=id)
    if not todo:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Item not found")
    if not crud.user.is_superuser(current_user) \
            and (todo.user_id != current_user.id):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Not enough permissions")
    todo = crud.todo.update(db=db, db_obj=todo, obj_in=item_in)

    return todo


@router.get("/{id}", response_model=schemas.TodoItem)
def read_todo(
        *,
        db: Session = Depends(get_session),
        id: int = __valid_id,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Get todo by ID.
    """
    todo = crud.todo.get(db=db, id=id)
    if not todo:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Item not found")
    if not crud.user.is_superuser(current_user) \
            and (todo.user_id != current_user.id):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Not enough permissions")

    return todo


@router.delete("/{id}", response_model=schemas.TodoItem)
def delete_todo(
        *,
        db: Session = Depends(get_session),
        id: int = __valid_id,
        current_user: models.User = Depends(deps.get_current_active_user),
) -> Any:
    """
    Delete an todo.
    """
    todo = crud.todo.get(db=db, id=id)
    if not todo:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="Item not found")
    if not crud.user.is_superuser(current_user) \
            and (todo.user_id != current_user.id):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Not enough permissions")
    todo = crud.todo.remove(db=db, id=id)

    return todo
