from jwt import JWT
from jwt.jwk import OctetJWK
from fastapi import Depends, HTTPException, status
from fastapi.security import HTTPBearer, HTTPAuthorizationCredentials
from pydantic import ValidationError
from sqlalchemy.orm import Session

from app import crud, models, schemas
from app.core import security
from app.core.config import settings
from app.db.db import get_session


reusable_httpbearer = HTTPBearer()
jwt = JWT()


def get_current_user(
        db: Session = Depends(get_session),
        token: HTTPAuthorizationCredentials = Depends(reusable_httpbearer)
) -> models.User:
    try:
        key = OctetJWK(settings.SECRET_KEY)
        payload = jwt.decode(
            token.credentials, key, algorithms=security.ALGORITHM
        )

        token_data = schemas.TokenPayload(**payload)
    except (ValidationError, Exception):
        raise HTTPException(
            status_code=status.HTTP_403_FORBIDDEN,
            detail="Could not validate credentials",
        )

    user = crud.user.get(db, id=token_data.sub)
    if not user:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND,
                            detail="User not found")

    return user


def get_current_active_user(
        current_user: models.User = Depends(get_current_user),
) -> models.User:
    if not crud.user.is_active(current_user):
        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail="Inactive user")

    return current_user
