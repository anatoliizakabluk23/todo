from sqlalchemy import Column, Integer, String, Boolean, ForeignKey
from sqlalchemy.orm import relationship
from geoalchemy2 import Geometry

from app.models.base import Base, TimestampMixin


class User(TimestampMixin, Base):
    __tablename__ = 'users'

    name = Column(String)
    username = Column(String)
    email = Column(String, unique=True, index=True)
    phone = Column(String)
    website = Column(String)
    hashed_password = Column(String, nullable=False)
    is_active = Column(Boolean(), default=True)
    is_superuser = Column(Boolean(), default=False)
    address = relationship("Address", uselist=False, lazy='joined',
                           backref="user")
    company = relationship("Company", uselist=False, lazy='joined',
                           backref="user")
    todos = relationship("Todo", back_populates="user")


class Address(Base):
    street = Column(String)
    suite = Column(String)
    city = Column(String)
    zipcode = Column(String)
    user_id = Column(Integer, ForeignKey('users.id'))
    geo = Column(Geometry('POINT', 4326))


class Company(Base):
    name = Column(String)
    catchPhrase = Column(String)
    bs = Column(String)
    user_id = Column(Integer, ForeignKey('users.id'))
