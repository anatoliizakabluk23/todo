# Import all the models, so that Base has them before creating tables
from app.models.base import Base # noqa
from app.models import user # noqa
from app.models import todo # noqa
