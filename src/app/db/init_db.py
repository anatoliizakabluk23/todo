import requests
import logging

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session
from sqlalchemy.exc import IntegrityError
from pydantic import ValidationError
from passlib.context import CryptContext

from app import schemas, models
from app.db.db import engine
from app.models.base import Base
# Import all the models, so that Base has them before creating tables
from app.db import base  # noqa: F401


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")

USERS_DATA_URL = 'https://jsonplaceholder.typicode.com/users'
TODOS_DATA_URL = 'https://jsonplaceholder.typicode.com/todos'
TEST_PASSWORD = 'root'


def get_external_users_data():
    try:
        data = requests.get(url=USERS_DATA_URL)

        return data.json()
    except Exception as e:
        logger.error(f'External service unavailable.\n {str(e)}')
        raise e


def get_external_todos_data():
    try:
        data = requests.get(url=TODOS_DATA_URL)

        return data.json()
    except Exception as e:
        logger.error(f'External service unavailable.\n {str(e)}')
        raise e


def init_users(db: Session):
    logger.info("Creating users data")

    if db.query(models.User).count() > 0:
        logger.warning("Users table has already had data.")
        return

    users = get_external_users_data()

    for user in users:
        user['password'] = TEST_PASSWORD
        try:
            user_in = schemas.UserCreate(**user)
        except ValidationError:
            continue

        try:
            obj_in_data = jsonable_encoder(user_in)
            obj_in_data['id'] = user['id']

            obj_in_data['hashed_password'] = pwd_context.hash(user_in.password)
            del obj_in_data['password']

            if user_in.address:
                obj_in_data['address'] = models.Address(
                    **jsonable_encoder(user_in.address))
            if user_in.company:
                obj_in_data['company'] = models.Company(
                    **jsonable_encoder(user_in.company))

            db_obj = models.User(**obj_in_data)
            db.add(db_obj)

            db.commit()

        except IntegrityError as ie:
            logger.error(ie._message())
            db.rollback()
            db.connection()

    logger.info("User data created")


def init_totos(db: Session):
    logger.info("Creating todos data")

    if db.query(models.Todo).count() > 0:
        logger.warning("Users table has already had data.")
        return

    todos = get_external_todos_data()

    for todo in todos:
        try:
            todo_in = schemas.TodoCreate(**todo)
        except ValidationError:
            continue

        obj_in_data = jsonable_encoder(todo_in)
        if 'userId' in todo:
            obj_in_data['user_id'] = todo['userId']

        db_obj = models.Todo(**obj_in_data)
        db.add(db_obj)

        try:
            db.commit()
        except IntegrityError as ie:
            logger.error(ie._message())
            db.rollback()
            db.connection()

    logger.info("Todos data created")


def init_db(db: Session) -> None:
    Base.metadata.create_all(bind=engine)

    init_users(db)
    init_totos(db)
