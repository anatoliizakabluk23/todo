from fastapi import status
from fastapi.testclient import TestClient

from app.core.config import settings
from app.tests.utils import user


def test_get_access_token(client: TestClient) -> None:

    login_data = {
        "email": user.EMAIL_TEST_USER,
        "password": user.PASSWORD_TEST_USER,
    }
    r = client.post(f"{settings.API_V1_STR}/login/access-token",
                    json=login_data)
    tokens = r.json()
    assert r.status_code == status.HTTP_200_OK
    assert "access_token" in tokens
    assert tokens["access_token"]
    assert "token_type" in tokens
    assert tokens["token_type"] == 'bearer'
