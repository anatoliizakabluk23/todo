from fastapi import status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app.core.config import settings
from app.tests.utils.todo import create_random_todo


def test_read_todos(
        client: TestClient, user_token_headers: dict, db: Session
) -> None:
    r = client.get(
        f"{settings.API_V1_STR}/todos/",
        headers=user_token_headers,
        params={'limit': 2, 'completed': False}
    )
    content = r.json()
    assert r.status_code == status.HTTP_200_OK
    assert len(content) == 2
    assert not content[0]['completed']

    r = client.get(
        f"{settings.API_V1_STR}/todos/",
        headers=user_token_headers,
        params={'limit': 1, 'completed': True}
    )
    content = r.json()
    assert r.status_code == status.HTTP_200_OK
    assert len(content) == 1
    assert content[0]['completed']


def test_create_todo(
        client: TestClient, user_token_headers: dict, db: Session
) -> None:
    data = {"title": "Foo"}
    r = client.post(
        f"{settings.API_V1_STR}/todos/",
        headers=user_token_headers,
        json=data,
    )
    assert r.status_code == status.HTTP_200_OK
    content = r.json()
    assert content["title"] == data["title"]
    assert not content["completed"]
    assert "id" in content
    assert "user_id" in content


def test_read_not_own_todo(
        client: TestClient, user_token_headers: dict, db: Session
) -> None:
    todo = create_random_todo(db)
    r = client.get(
        f"{settings.API_V1_STR}/todos/{todo.id}", headers=user_token_headers,
    )

    assert r.status_code == status.HTTP_400_BAD_REQUEST


def test_update_todo(
        client: TestClient,
        initial_data: dict,
        user_token_headers: dict,
        db: Session
) -> None:
    user = initial_data['user']
    todo = user.todos[0]

    new_title = 'Test title'
    r = client.put(
        f"{settings.API_V1_STR}/todos/{todo.id}",
        headers=user_token_headers,
        json={'title': new_title}
    )
    context = r.json()
    assert r.status_code == status.HTTP_200_OK
    assert context['title'] == new_title

    todo = create_random_todo(db)
    r = client.put(
        f"{settings.API_V1_STR}/todos/{todo.id}",
        headers=user_token_headers,
        json={'title': new_title}
    )
    assert r.status_code == status.HTTP_400_BAD_REQUEST

    r = client.put(
        f"{settings.API_V1_STR}/todos/{100110100101}",
        headers=user_token_headers,
        json={'title': new_title}
    )
    assert r.status_code == status.HTTP_404_NOT_FOUND


def test_delete_todo(
        client: TestClient,
        initial_data: dict,
        user_token_headers: dict,
        db: Session
) -> None:
    user = initial_data['user']
    todo = user.todos[0]
    count_before = len(user.todos)

    r = client.delete(
        f"{settings.API_V1_STR}/todos/{todo.id}",
        headers=user_token_headers,
    )
    assert r.status_code == status.HTTP_200_OK
    db.refresh(user)
    count_after = len(user.todos)
    assert count_after == (count_before - 1)

    todo = create_random_todo(db)
    r = client.delete(
        f"{settings.API_V1_STR}/todos/{todo.id}",
        headers=user_token_headers,
    )
    assert r.status_code == status.HTTP_400_BAD_REQUEST

    r = client.delete(
        f"{settings.API_V1_STR}/todos/{100110100101}",
        headers=user_token_headers,
    )
    assert r.status_code == status.HTTP_404_NOT_FOUND
