from typing import Dict

from fastapi import status
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session
from geoalchemy2.shape import to_shape

from app import crud
from app.core.config import settings
from app.tests.utils.user import (
    user_authentication_headers, create_user,
    EMAIL_TEST_USER,)
from app.tests.utils.utils import random_email, random_lower_string


def test_get_users(client: TestClient) -> None:
    r = client.get(f"{settings.API_V1_STR}/users/")
    users = r.json()
    assert r.status_code == status.HTTP_200_OK
    assert len(users) > 0


def test_get_users_user_me(
        client: TestClient, user_token_headers: Dict[str, str]
) -> None:
    r = client.get(
        f"{settings.API_V1_STR}/users/me", headers=user_token_headers,)

    current_user = r.json()
    assert current_user
    assert current_user["is_active"] is True
    assert current_user["email"] == EMAIL_TEST_USER


def test_get_existing_user(
    client: TestClient, user_token_headers: dict, db: Session
) -> None:
    email = random_email()
    password = random_lower_string()
    user = create_user(db, email=email, password=password)

    user_id = user.id
    r = client.get(
        f"{settings.API_V1_STR}/users/{user_id}",
        headers=user_token_headers,
    )
    assert r.status_code == status.HTTP_400_BAD_REQUEST

    headers = user_authentication_headers(
        client=client, email=user.email, password=password)

    r = client.get(f"{settings.API_V1_STR}/users/{user_id}", headers=headers)

    assert (status.HTTP_200_OK <= r.status_code
            < status.HTTP_300_MULTIPLE_CHOICES)
    api_user = r.json()
    existing_user = crud.user.get_by_email(db, email=email)
    assert existing_user
    assert existing_user.email == api_user["email"]


def test_update_user_me(client: TestClient, db: Session) -> None:
    email = random_email()
    password = random_lower_string()
    user = create_user(db, email=email, password=password)

    headers = user_authentication_headers(
        client=client, email=user.email, password=password)

    data = {
        'username': 'testname',
        'company': {
            'name': 'Test Company',
        },
        'address': {
            'geo': {
                'lng': 81.1496,
                'lat': -37.3159
            },
        },
    }
    r = client.put(
        f"{settings.API_V1_STR}/users/me",
        json=data,
        headers=headers,
    )

    updated_user = r.json()
    assert r.status_code == status.HTTP_200_OK
    db.refresh(user)
    assert user.username == updated_user['username']
    assert user.company.name == updated_user['company']['name']
    s_geom = to_shape(user.address.geo)
    assert {'lng': s_geom.x, 'lat': s_geom.y} == updated_user['address']['geo']


def test_reset_password(
        client: TestClient, db: Session
) -> None:
    email = random_email()
    password = random_lower_string()
    user = create_user(db, email=email, password=password)

    headers = user_authentication_headers(
        client=client, email=user.email, password=password)

    new_password = random_lower_string()
    new_password_repeat = random_lower_string()

    data = {
        'old_password': password,
        'new_password': new_password,
        'new_password_repeat': new_password_repeat,
    }

    r = client.patch(
        f"{settings.API_V1_STR}/users/me/update-password/",
        json=data,
        headers=headers
    )
    assert r.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY

    data = {
        'old_password': 'random',
        'new_password': new_password,
        'new_password_repeat': new_password,
    }

    r = client.patch(
        f"{settings.API_V1_STR}/users/me/update-password/",
        json=data,
        headers=headers
    )
    assert r.status_code >= status.HTTP_400_BAD_REQUEST

    data = {
        'old_password': password,
        'new_password': new_password,
        'new_password_repeat': new_password,
    }

    r = client.patch(
        f"{settings.API_V1_STR}/users/me/update-password/",
        json=data,
        headers=headers
    )

    assert r.status_code == status.HTTP_200_OK
    assert r.json() == {"msg": "Password updated successfully"}
