from typing import Dict, Generator

import pytest
from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app import crud, schemas
from app.main import Base
from app.tests.utils.user import (authentication_token_from_email,
                                  EMAIL_TEST_USER, PASSWORD_TEST_USER)
from app.tests.test_app import override_get_db, test_client, engine
from app.tests.utils.utils import load_json_fixture


@pytest.fixture(scope="session")
def db() -> Generator:
    yield from override_get_db()
    Base.metadata.drop_all(bind=engine)


@pytest.fixture(scope="module")
def client() -> TestClient:
    with test_client as c:
        yield c


@pytest.fixture(scope="module")
def user_token_headers(client: TestClient, db: Session) -> Dict[str, str]:
    return authentication_token_from_email(client=client, db=db)


@pytest.fixture(scope='session', autouse=True)
def initial_data(db):
    user_data = load_json_fixture('user.json')
    user_data['password'] = PASSWORD_TEST_USER
    user_data['email'] = EMAIL_TEST_USER

    user_in_create = schemas.UserCreate(**user_data)
    user = crud.user.create(db, obj_in=user_in_create)

    todos = load_json_fixture('todo.json')
    for todo in todos:
        todo_in_create = schemas.TodoCreate(**todo)
        crud.todo.create_with_owner(db, obj_in=todo_in_create, user_id=user.id)

    yield {'user': user}
