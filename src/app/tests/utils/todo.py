from typing import Optional

from sqlalchemy.orm import Session

from app import crud, models
from app.schemas.todo import TodoCreate
from app.tests.utils.user import create_random_user
from app.tests.utils.utils import random_lower_string


def create_random_todo(
        db: Session,
        *,
        user_id: Optional[int] = None,
        completed: bool = False
) -> models.Todo:
    if user_id is None:
        user = create_random_user(db)
        user_id = user.id
    title = random_lower_string()
    todo_in = TodoCreate(title=title, completed=completed)

    return crud.todo.create_with_owner(db=db, obj_in=todo_in, user_id=user_id)
