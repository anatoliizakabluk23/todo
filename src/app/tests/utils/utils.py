import os
import json
import random
import string


def random_lower_string() -> str:
    return "".join(random.choices(string.ascii_lowercase, k=32))


def random_email() -> str:
    return f"{random_lower_string()}@{random_lower_string()}.com"


def load_json_fixture(file):
    """
    Load json fixture file from the disc.
    """

    file_path = os.path.join('.', 'app', 'tests', 'fixtures', file)
    with open(file_path) as f:
        return json.load(f)
