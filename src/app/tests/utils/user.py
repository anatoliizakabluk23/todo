from typing import Dict

from fastapi.testclient import TestClient
from sqlalchemy.orm import Session

from app import crud
from app.core.config import settings
from app.models.user import User
from app.schemas.user import UserCreate
from app.tests.utils.utils import load_json_fixture
from app.tests.utils.utils import random_email, random_lower_string


EMAIL_TEST_USER = "test@example.com"
PASSWORD_TEST_USER = "test"


def user_authentication_headers(
        *, client: TestClient, email: str, password: str
) -> Dict[str, str]:
    data = {"email": email, "password": password}

    r = client.post(
        f"{settings.API_V1_STR}/login/access-token",
        json=data,
        headers={'Content-Type': 'application/json'}
    )
    response = r.json()

    auth_token = response["access_token"]
    headers = {"Authorization": f"Bearer {auth_token}"}

    return headers


def create_user(db: Session, *, email: str, password: str) -> User:
    user_data = load_json_fixture('user.json')
    user_data['password'] = password
    user_data['email'] = email
    user_in = UserCreate(**user_data)
    user = crud.user.create(db=db, obj_in=user_in)

    return user


def create_random_user(db: Session) -> User:
    email = random_email()
    password = random_lower_string()
    user_data = load_json_fixture('user.json')
    user_data['password'] = password
    user_data['email'] = email
    user_in = UserCreate(**user_data)
    user = crud.user.create(db=db, obj_in=user_in)

    return user


def authentication_token_from_email(
        *,
        client: TestClient,
        email: str = EMAIL_TEST_USER,
        password: str = PASSWORD_TEST_USER,
        db: Session
) -> Dict[str, str]:
    """
    Return a valid token for the user with given email.

    If the user doesn't exist it is created first.
    """
    user = crud.user.get_by_email(db, email=email)
    if not user:
        user_data = load_json_fixture('user.json')
        user_data['password'] = password
        user_data['email'] = email

        user_in_create = UserCreate(**user_data)
        crud.user.create(db, obj_in=user_in_create)

    return user_authentication_headers(
        client=client, email=email, password=password)
