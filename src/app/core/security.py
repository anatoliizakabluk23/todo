from typing import Any, Union
from datetime import datetime, timedelta, timezone

from jwt import JWT
from jwt.jwk import OctetJWK
from jwt.utils import get_int_from_datetime
from fastapi import HTTPException
from passlib.context import CryptContext
from sqlalchemy.orm import Session

from app.core.config import settings
from app import crud


pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
jwt = JWT()

ALGORITHM = "HS256"


def authenticate(db: Session, *, email: str, password: str) -> str:
    """
    HTTPBearer compatible token login, get an access token for future requests
    """
    user = crud.user.get_by_email(db, email=email)

    if not user:
        raise HTTPException(status_code=400, detail="Incorrect email")

    if not verify_password(password, user.hashed_password):
        raise HTTPException(status_code=400, detail="Incorrect password")

    access_token = create_access_token(user.id)

    return access_token


def create_access_token(
        subject: Union[str, Any], expires_delta: timedelta = None
) -> str:
    if expires_delta:
        expire = datetime.utcnow() + expires_delta
    else:
        expire = datetime.utcnow() + timedelta(
            minutes=settings.ACCESS_TOKEN_EXPIRE_MINUTES
        )

    to_encode = {
        'sub': str(subject),
        'iat': get_int_from_datetime(datetime.now(timezone.utc)),
        'exp': get_int_from_datetime(expire),
    }

    key = OctetJWK(settings.SECRET_KEY)
    encoded_jwt = jwt.encode(to_encode, key, alg=ALGORITHM)

    return encoded_jwt


def verify_password(plain_password: str, hashed_password: str) -> bool:
    return pwd_context.verify(plain_password, hashed_password)


def get_password_hash(password: str) -> str:
    return pwd_context.hash(password)
