from typing import Any, Dict, Union, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.core.security import get_password_hash
from app.crud.base import CRUDBase
from app.models.user import User, Company, Address
from app.schemas.user import UserCreate, UserUpdate


class CRUDUser(CRUDBase[User, UserCreate, UserUpdate]):
    def get_by_email(self, db: Session, *, email: str) -> Optional[User]:
        return db.query(User).filter(User.email == email).first()

    def create(self, db: Session, *, obj_in: UserCreate):
        obj_in_data = jsonable_encoder(obj_in)

        obj_in_data['hashed_password'] = get_password_hash(obj_in.password)
        del obj_in_data['password']

        if obj_in.address:
            obj_in_data['address'] = Address(
                **jsonable_encoder(obj_in.address))
        if obj_in.company:
            obj_in_data['company'] = Company(
                **jsonable_encoder(obj_in.company))

        db_obj = User(**obj_in_data)
        db.add(db_obj)

        db.commit()
        db.refresh(db_obj)

        return db_obj

    def update(
            self,
            db: Session,
            *,
            db_obj: User,
            obj_in: Union[UserUpdate, Dict[str, Any]]
    ) -> User:

        if isinstance(obj_in, dict):
            update_data = obj_in
        else:
            update_data = obj_in.dict(exclude_unset=True)

        if 'password' in update_data:
            hashed_password = get_password_hash(update_data["password"])
            del update_data["password"]
            update_data["hashed_password"] = hashed_password

        for field, value in update_data.items():
            try:
                getattr(db_obj, field)
            except AttributeError:
                continue
            if isinstance(value, dict):
                sub_model = getattr(db_obj, field)
                for sub_field in value:
                    try:
                        getattr(db_obj, field)
                    except AttributeError:
                        continue
                    setattr(sub_model, sub_field,
                            update_data[field][sub_field])
                setattr(db_obj, field, sub_model)
            else:
                setattr(db_obj, field, update_data[field])

        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)

        return db_obj

    def is_active(self, user: User) -> bool:
        return user.is_active

    def is_superuser(self, user: User) -> bool:
        return user.is_superuser


user = CRUDUser(User)
