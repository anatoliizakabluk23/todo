from typing import List, Dict, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.orm import Session

from app.crud.base import CRUDBase
from app.models.todo import Todo
from app.schemas.todo import TodoCreate, TodoUpdate


class CRUDTodo(CRUDBase[Todo, TodoCreate, TodoUpdate]):
    def create_with_owner(
            self, db: Session, *, obj_in: TodoCreate, user_id: int
    ) -> Todo:
        obj_in_data = jsonable_encoder(obj_in)
        db_obj = self.model(**obj_in_data, user_id=user_id)
        db.add(db_obj)
        db.commit()
        db.refresh(db_obj)

        return db_obj

    def get_multi_by_owner(
            self,
            db: Session,
            *, user_id: int,
            skip: int = 0,
            limit: int = 100,
            filters: Optional[Dict] = None
    ) -> List[Todo]:
        query = db.query(self.model)
        if filters:
            query = query.filter_by(**filters)

        return (
            query
            .filter(Todo.user_id == user_id)
            .offset(skip)
            .limit(limit)
            .all()
        )


todo = CRUDTodo(Todo)
